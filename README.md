###Getting Started###

Install NodeJS
```
https://nodejs.org/en/download/
```

Install Git
```
https://git-scm.com/downloads
```

Checkout this repo, install dependencies, then start the app with the following:

```
	> git clone git@bitbucket.org:barrylavides/ahub.git
	> cd ahub
	> npm install
	> npm start
```
Open http://localhost:8080/ in your browser
